
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:27PM

See merge request itentialopensource/adapters/adapter-salesforce_cases!13

---

## 0.5.3 [09-16-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-salesforce_cases!11

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_18:39PM

See merge request itentialopensource/adapters/adapter-salesforce_cases!10

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_19:51PM

See merge request itentialopensource/adapters/adapter-salesforce_cases!9

---

## 0.5.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_cases!8

---

## 0.4.4 [03-27-2024]

* Changes made at 2024.03.27_14:03PM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_cases!7

---

## 0.4.3 [03-13-2024]

* Changes made at 2024.03.13_15:05PM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_cases!6

---

## 0.4.2 [03-11-2024]

* Changes made at 2024.03.11_14:45PM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_cases!5

---

## 0.4.1 [02-28-2024]

* Changes made at 2024.02.28_11:10AM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_cases!4

---

## 0.4.0 [12-31-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_cases!3

---

## 0.3.0 [05-24-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_cases!2

---

## 0.2.0 [07-28-2021]

- Adding updateCase

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_cases!1

---

## 0.1.2 [07-19-2021] & 0.1.1 [07-19-2021]

- Initial Commit

See commit c173624

---
