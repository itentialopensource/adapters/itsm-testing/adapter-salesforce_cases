# Saleforce Cases

Vendor: Saleforce
Homepage: https://www.salesforce.com/

Product: Service Cloud
Product Page: https://www.salesforce.com/service/cloud/

## Introduction
We classify Saleforce Cases into the ITSM (Service Management) domain as Saleforce Cases provides the capabilities that align with ITSM processes, such as ticket management, service request management, and performance monitoring.

## Why Integrate
The Saleforce Cases adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Saleforce Service Cloud to support and enhance the IT service management processes. With this adapter you have the ability to perform operations such as:

- Get, Create, or Update Ticket

## Additional Product Documentation
The [API documents for Saleforce Cases](https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/intro_rest.htm)