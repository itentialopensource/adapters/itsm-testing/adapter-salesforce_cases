
## 0.3.0 [05-24-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_cases!2

---

## 0.2.0 [07-28-2021]

- Adding updateCase

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_cases!1

---

## 0.1.2 [07-19-2021] & 0.1.1 [07-19-2021]

- Initial Commit

See commit c173624

---
